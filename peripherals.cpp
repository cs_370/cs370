#include <stdlib.h>

#include "miniat.h"
#include "peripherals.h"


#define P_JUMP (0x4000)
#define P_OBSTACLE_AHEAD (0x4001)
/*
#define P_WEAPON (0x4010)
#define P_WEAPON_TURN (0x4011)
#define P_WEAPON_AIM (0x4012)
*/

void peripherals_cleanup(void) {

	/* nothing yet */

	return;
}

void peripherals_clock(miniat *m) {

	m_bus bus;

	if(!m) {
		fprintf(stderr, "NULL MiniAT on peripheral clock\n");
		exit(EXIT_FAILURE);
	}

	bus = miniat_pins_bus_get(m);

	if(bus.req == M_HIGH && bus.ack == M_LOW) {
		/*
		 * The MiniAT is waiting for a peripheral acknowledgement
		 */
		bool handled = true;
		if(bus.rW == M_HIGH) {

			/* STOR to peripheral */

			switch(bus.address) {
			case P_JUMP:
				printf("STORed %d to the P_JUMP peripheral\n", (signed)bus.data);
				break;
			case P_OBSTACLE_AHEAD:
				printf("STORed %d to the P_OBSTACLE_AHEAD peripheral\n", (signed)bus.data);
				break;
			default:
				fprintf(stderr, "There is no peripheral accepting writes at 0x%04X\n", bus.address);
				/*
				 * Here, it would be good to set "handled" to false so the ACK bus
				 * line isn't set high automatically.  It may hang the system, but
				 * devices that aren't there shouldn't be communicated with.
				 */
				break;
			}
		}
		else {

			/* LOAD from peripheral */

			switch(bus.address) {
				case P_JUMP:
					printf("LOADed from P_JUMP\n");
					break;
				case P_OBSTACLE_AHEAD:
					printf("LOADed from P_TURN_ANGLE\n");
					break;
				default:
					fprintf(stderr, "There is no peripheral accepting reads at 0x%04X\n", bus.address);
					break;
			}
		}

		if(handled) {
			bus.ack = M_HIGH;
		}
	}
	else if(bus.ack) {
		/*
		 * The MiniAT is waiting for the peripheral to lower the ACK pin.
		 * Devices intended to take longer than normal should add delay
		 * "case"s here.
		 */
		switch(bus.address) {
			default:
				bus.ack = M_LOW;
				break;
		}
	}

	miniat_pins_bus_set(m, bus);

	return;
}
