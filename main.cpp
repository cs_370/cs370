//g++ file.cpp -I/usr/include/SDL2 -lSDL2 -lSDL2_image

#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <signal.h>
#include <stdint.h>
#include <inttypes.h>
#include <exception>

#include "miniat.h"
#include "peripherals.h"
#include "ports.h"
#include "game.h"


#define MAX_CYCLES UINT64_MAX


miniat *m = NULL;
char *input_filename = NULL;
FILE *infile = NULL;

uint64_t cycles = 0;


static void cleanup(void);
static void miniat_start(int argc, char *argv[]);
static void parse_options(int argc, char *argv[]);
static void print_usage(void);
static void signal_handler(int sig);


int main(int argc, char *argv[]) {

	try {
		miniat_start(argc, argv);
	}
	catch(std::exception &e) {
		miniat_dump_error();
	}

	return EXIT_SUCCESS;
}


static void miniat_start(int argc, char *argv[]) {

	signal(SIGINT, signal_handler);
	signal(SIGTERM, signal_handler);

	atexit(cleanup);

	parse_options(argc, argv);

	infile = fopen(input_filename, "r+b");
	if(!infile) {
		fprintf(stderr, "Couldn't open \"%s\".  %s", input_filename, strerror(errno));
		exit(EXIT_FAILURE);
	}

	if( !game_init() ) {
		printf( "Failed to initialize SDL!\n" );
		exit(EXIT_FAILURE);
	}

	if( !loadMedia() ) {
		printf( "Failed to load media!\n" );
		exit(EXIT_FAILURE);
	}

	m = miniat_new(infile, NULL);

	for(;;) {
		if(cycles < MAX_CYCLES) {
			cycles++;
		}

		miniat_clock(m);
		peripherals_clock(m);
		ports_clock(m);
		game_clock();
	}

	return;
}


static void signal_handler(int sig) {

	if(sig == SIGINT || sig == SIGTERM) {
		exit(EXIT_SUCCESS);
	}

	return;
}

static void cleanup(void) {

	if(m) {
		/* MiniAT also closes the binary file it was passed on miniat_new */
		miniat_free(m);
	}

	/*
	 * Call all the other cleanup functions
	 */
	peripherals_cleanup();
	ports_cleanup();
	game_cleanup();

	if(cycles < MAX_CYCLES) {
		printf("\n%"PRIu64" cycles executed\n", cycles);
	}
	else {
		printf("Runtime exceeded %"PRIu64" cycles!\n", MAX_CYCLES);
	}

	return;
}


/*
 * print_usage()
 *
 * Display command and option usage
 */
static void print_usage(void) {

	fprintf(stderr, "\n");
	fprintf(stderr, "Usage:  "EXECUTABLE" [bin_file]\n");
	fprintf(stderr, "\n");

	return;
}


static void parse_options(int argc, char *argv[]) {

	if(argc != 2) {
		print_usage();
		exit(EXIT_FAILURE);
	}

	input_filename = argv[1];

	return;
}
