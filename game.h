#ifndef __GAME_H__
#define __GAME_H__

#include "miniat.h"

extern void game_cleanup(void);
extern void game_clock(void);
extern bool game_init(void);
extern bool loadMedia(void);
extern void game_cleanup(void);

#endif /* __GAME_H__ */
