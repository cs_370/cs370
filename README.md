# Heli2 - CS 370
#### System intent: Build a programmable platformer simulation to test your programming and problem solving skills.
---

To use this project you need to have the **The Virtual Embedded Architecture Project - MiniAT** located here: http://www.miniat.org/

These are the commands to download and build MiniAT:

```bash
git clone https://bitbucket.org/miniat/0x1-miniat.git ~/miniat
sudo apt-get install make gcc scons python flex libncurses5-dev doxygen sqlite3
cd ~/miniat
scons
```

From there all you need to do is:

* download this project
* put it in **MiniAT systems folder**
* run **scons** from the MiniAT root directory
* then run the program with the MiniAT bin file.

These are the commands for doing so:

```bash
git clone git@bitbucket.org:cs_370/cs370.git ~/miniat/system/heli
cd ~/miniat
scons
heli ./out/bin/heli.bin
```

This project was built and tested on Ubuntu 16.04, so whether it works on other systems is unknown.
