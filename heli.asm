# peripheral constants

.const P_OBSTACLE_AHEAD 0x4000 # dist to closest obstacle in path, or <0
.const P_JUMP 0x4001 # store 0--100

.const MAX_JUMP 100
.const JUMP_DIST 50
.const PROBABILITY 1000 # random chance to jump is 1:1000

####################
# REGISTERS
#
# r100: distance to closest obstacle in path
# r101: jump threshold distance for comparisons
#
# !rand
# r120: return address
# r121: returned random bit string
# r122--124: internal seed history
####################

!main
	movi r101 = JUMP_DIST
	movi  r122 = 0xDEADC33D
	movi  r123 = 0xBADCC0DE

!maybe_jump

# see if we just feel like jumping
	add   r120 = PC + 2
	bra   [!rand]
	mod   r121 = r121 % PROBABILITY
	brane [!lookout], r121 != r0
# we decided to randomly jump a random amount!
	add  r120 = PC + 2
	bra  [!rand]
	mod  r121 = r121 % MAX_JUMP
	stor [P_JUMP] = r121

!lookout
	load r100 = [P_OBSTACLE_AHEAD]
	brag [!maybe_jump], r100 > r101

# Obstacle ahead!  Jump with the same power as distance... just a guess :-)
	stor [P_JUMP] = r100
	bra  [!lookout]

!inf
	bra [!inf]


<<<
	!rand

	random bit pattern returned in r121

	uses registers r122...r124
	return addres expected in r120
>>>


!rand
	movr r124 = r122
	shl  r122 = r122 << 7
	shr  r124 = r124 >> 25
	exor r122 = r122 ^ r124
	add  r122 = r122 + r123
	movr r121 = r122

	movr r124 = r123
	shl  r123 = r123 << 7
	shr  r124 = r124 >> 25
	exor r123 = r123 ^ r124
	add  r123 = r123 + 0xBADCC0DE

	bra [r120]
